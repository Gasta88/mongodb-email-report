#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Collection of objects and methods to generate the email."""

from assa_email_reporter import config
import logging
import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders
import os

logger = logging.getLogger('file_logger')


class EmailHandler():
    """Class that format and send the email."""

    def __init__(self, receivers=[], sender=[], text=None, subject=None):
        """Initialize EmailHandler object."""
        self.receivers = receivers
        self.sender = sender
        self.subject = str(subject)
        self.text = text
        self.att_file = None

    def add_text(self, json_text):
        """Add text on a new line to the message."""
        self.att_file = os.path.join('data', 'ASSA_run_export.csv')
        with open(self.att_file, 'w') as f:
            f.write("RUNID,EXPERIMENTS,RUNDATE,RUNPATH\n")
            for d in json_text:
                line = ",".join(d.values()) + "\n"
                f.write(line)
        self.text = "Please see attachments."

    def send(self):
        """Send the notification via email."""
        msg = MIMEMultipart()
        msg['From'] = self.sender
        msg['To'] = self.receivers
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = self.subject

        msg.attach(MIMEText(self.text))
        part = MIMEBase('application', "octet-stream")
        with open(self.att_file, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(
                            Path(self.att_file).name))
        msg.attach(part)
        server = smtplib.SMTP(config['mail_smtp_server'])
        server.sendmail(config['mail_from_address'],
                        self.receivers,
                        msg.as_string())
        server.quit()
        os.remove(self.att_file)
        return
