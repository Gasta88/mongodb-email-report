#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Used to import the assa_email_reporter module."""

import logging
import os


def read_config(filename):
    """Return application configuration dictionary.

    Parameters
    ----------
    filename : str
        Location of the application configuration file.

    Returns
    -------
    configDict : dict
        Dictionary with all the settings for the application as defined on
        application start.

    """
    configDict = {}
    with open(filename, 'r') as f:
        content = f.readlines()

    for line in content:
        line = line.strip()

        # ignore empty lines, comments and lines without value
        if line == '' or line.startswith('#') or '=' not in line:
            continue

        try:
            key, val = line.split('=', 1)
            key = key.strip()
            val = val.strip().strip('"')

            # type specific casting
            if val.isdigit():
                configDict[key] = int(val)

            elif val.startswith('~'):
                configDict[key] = os.path.expanduser(val)

            else:
                configDict[key] = val

        except ValueError:
            continue

    return configDict


def setup_logging(logFile=None, cmdline=True, level='info'):
    """Set up the logging file and the level of information displayed.

    Parameters
    ----------
    logFile : str
        Location of the log file stored inside the configuration file.

    cmdLine: boolean
        add a stream handler to log to the command line (if True)
    level : str
        Logging level of information.

    Returns
    -------
    logging.Logger
        Handle to the log file where to write info during the process.

    """
    level = {'debug': logging.DEBUG,
             'info': logging.INFO,
             'warning': logging.WARNING,
             'error': logging.ERROR}.get(level.lower())

    form = ('%(asctime)s: [%(levelname)s]: %(filename)s in '
            '%(funcName)s (%(lineno)d): %(message)s')
    logger = logging.getLogger('file_logger')
    logger.setLevel(level)
    if cmdline:
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter(form))
        ch.setLevel(level)
        logger.addHandler(ch)
        logger.debug('setup commandline logging level: ' + str(level))

    if logFile is not None:
        eh = logging.FileHandler(logFile, mode='a')
        eh.setLevel(logging.DEBUG)
        formatter = logging.Formatter(form)
        eh.setFormatter(formatter)
        logger.addHandler(eh)
        logger.debug('Set up file logging: ' + logFile)

    return logger


base = os.path.split(os.path.split(__file__)[0])[0]
CI_TEST = os.environ.get('CI_TEST', '0')

if CI_TEST == '1':
    print('Using CI testing environment')
    config = read_config(os.path.join(base, 'tests', 'config.cfg'))
else:
    config = read_config(os.path.join(base, 'config.cfg'))
