#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Collection of objects and methods to work with the ASSA MongoDB."""

from assa_email_reporter import config, CI_TEST
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
# from bson.objectid import ObjectId
import logging
import sys

logger = logging.getLogger('file_logger')


class MongoConnector():
    """Class that perform actions on the ASSA MongoDB."""

    def __init__(self):
        """Initialize MongoConnector object."""
        db_uri = config['mongo_uri']
        logger.info('Initialize MongoDB connection...')
        client = MongoClient(db_uri)
        try:
            client.admin.command('ismaster')
        except ConnectionFailure:
            logger.info('No server available.')
            sys.exit("Server not available")
        db_site = db_uri.split("?")[0][-3:].upper()
        logger.info('Currently at site: {}'.format(db_site))
        if CI_TEST == '0':
            if db_site == "SIG":
                self.db = client["assa_productive_sig"]
            else:
                self.db = client["assa_productive_sii"]
        else:
            self.db = client['assa_testing']

    def _clean_run_document(self, d):
        """Return a smaller Run document dict (PRIVATE)."""
        fields = ['_id', 'experiment_names', 'date', 'path']
        new_d = {}
        for key in d:
            if key in fields:
                if key == 'experiment_names':
                    list_experiments = d[key]
                    if len(list_experiments) == 1:
                        new_d[key] = list_experiments[0]
                    else:
                        sl_list_exps = [e for e in list_experiments
                                        if '-R0' in e]
                        new_d[key] = "; ".join(sl_list_exps)
                elif key == 'date':
                    new_d[key] = d[key].strftime('%Y-%m-%d')
                else:
                    new_d[key] = d[key]
        return new_d

    def get_runs(self):
        """Return a collection of runs from the ASSA MongoDB.

        The runs need to be from ServiceLab department, or from a pooled run.
        The run status must be 'done'.
        Return the flowcellid, the experiment ids, the run date and the bcl
        folder path.

        """
        if "Run" not in self.db.list_collection_names():
            logger.info('No Run collection found.')
            sys.exit('No Run collection inside database')
        self.run_collection = self.db['Run']
        self.documents = []
        for d in self.run_collection.find({'status': 'done',
                                           'experiment_names':
                                               {'$regex': '-R0'}}):
            self.documents.append(self._clean_run_document(d))
        logger.info(f'Found {len(self.documents)} documents.')
        return self.documents
