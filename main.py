#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Main script for the ASSA email report process."""

from assa_email_reporter import setup_logging, config
from assa_email_reporter.db_utils import MongoConnector
from assa_email_reporter.email_utils import EmailHandler


def main():
    """Run main part of the assa email reporter app."""
    logger.info('ASSA runs report begin.')
    email_subject = """List of Service Lab experiments available for
                        removal."""
    eh = EmailHandler(receivers=config['receivers'],
                      sender=config['mail_from_address'],
                      subject=email_subject)
    mc = MongoConnector()
    runs = mc.get_runs()
    eh.add_text(json_text=runs)
    eh.send()
    return


if __name__ == "__main__":
    logger = setup_logging(logFile='info.log',
                           level='info')
    main()
