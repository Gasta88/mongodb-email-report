# MongoDB email report

Project where I create an automated report for personal use. It connects to the MongoDB instance where my collections are hosted, query the *Run* collection and retrieve specific documents defined by a query.

Can be automatically scheduled via CRON (Unix) or Task Scheduler (Windows).
 
## Design

Connectivity via *[pymongo]([https://pypi.org/project/pymongo/](https://pypi.org/project/pymongo/))* tot he MongoDB instance. This document based database hold information about different runs performed across several departments.
Main interested is for those runs that are made for the Service Lab and are complete.

Documents are exported and changed into a tabulated format attached to an email. Such email is sent to a receiver (myself in this case) on a schedule that is not defined inside this project.

A configuration file holds all the necessary information to make the script run.

## Assumptions

- MongoDB instance and script are not on the same server.
- Multiple departments, but only one is of interest
- Query is hard-coded inside `assa_email_reporter/db_utils.py`. Greater extension and further flexibility in the query are out of this project scope.
- Basic email layout without too much formatting on top of it
- Email is sent whether there are documents or not.

## How to run

To run the application is it necessary to clone the repository locally and later schedule the `main.py` script via the preferred tool for task scheduling.

Python 3.7+ is a prerequisite for this project. Other dependencies are specified inside the `requirements.txt` file.
