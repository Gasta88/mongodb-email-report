#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Used to test assa_email_reporter.email_utils module."""

import unittest
from assa_email_reporter import config
from assa_email_reporter.email_utils import EmailHandler
from unittest.mock import patch


class EmailUtilsTestCase(unittest.TestCase):
    """Collect unit tests for assa_email_reporter.email_utils."""

    maxDiff = None

    def test_A_initialize(self):
        """Test initialization of EmailHandler object."""
        receivers = config['receivers']
        sender = config['mail_from_address']
        subject = "CI TEST"
        eh_object = EmailHandler(receivers=receivers,
                                 sender=sender,
                                 subject=subject)
        self.assertEqual(eh_object.receivers, config['receivers'])
        self.assertEqual(eh_object.sender, config['mail_from_address'])

    def test_B_format_text(self):
        """Test formatting of json text into a suitable tabulated format."""
        receivers = config['receivers']
        sender = config['mail_from_address']
        subject = "CI TEST"
        test_runs = [{'_id': 'id1', 'experiments': 'exp1;exp2',
                      'date': '2021-03-21', 'path': 'this/is/a/test/path'},
                     {'_id': 'id2', 'experiments': 'exp3',
                      'date': '2021-05-21',
                      'path': 'this/is/another/test/path'}]
        eh_object = EmailHandler(receivers=receivers,
                                 sender=sender,
                                 subject=subject)
        eh_object.add_text(json_text=test_runs)
        self.assertEqual(eh_object.text, "Please see attachments.")

    def test_C_send_message(self):
        """Test email transfer."""
        receivers = config['receivers']
        sender = config['mail_from_address']
        subject = "CI TEST"
        test_runs = [{'_id': 'id1', 'experiments': 'exp1;exp2',
                      'date': '2021-03-21', 'path': 'this/is/a/test/path'},
                     {'_id': 'id2', 'experiments': 'exp3',
                      'date': '2021-05-21',
                      'path': 'this/is/another/test/path'}]
        eh_object = EmailHandler(receivers=receivers,
                                 sender=sender,
                                 subject=subject)
        eh_object.add_text(json_text=test_runs)
        with patch("smtplib.SMTP") as mock_smtp:
            eh_object.send()
        # Get instance of mocked SMTP object
        instance = mock_smtp.return_value
        self.assertEqual(instance.sendmail.call_count, 1)


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=3)
    unittest.main(testRunner=runner)
