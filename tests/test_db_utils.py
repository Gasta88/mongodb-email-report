#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Used to test assa_email_reporter.db_utils module."""

import unittest
from assa_email_reporter.db_utils import MongoConnector


class DbUtilsTestCase(unittest.TestCase):
    """Collect unit tests for assa_email_reporter.db_utils."""

    maxDiff = None

    def setUp(self):
        """Set up environment for each test."""
        self.mongo_object = MongoConnector()

    def test_A_get_runs(self):
        """Test collection of runs from MongoDB."""
        results = self.mongo_object.get_runs()
        self.assertEqual(len(results), 3)


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=3)
    unittest.main(testRunner=runner)
