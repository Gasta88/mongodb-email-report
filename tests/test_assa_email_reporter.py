#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Allow the testing of the assa_email_reporter module."""

import unittest
from assa_email_reporter import read_config, setup_logging
import os


class AssaEmailReporterTestCase(unittest.TestCase):
    """Test initialization of assa_email_reporter module."""

    def test_read_config(self):
        """Test assa_email_reporter.read_config method."""
        referenceDict = {
            'mongo_uri': 'mongodb://mongo/assa_testing',
            'mail_smtp_server': 'syseucasarray01.sysmexd.sysmex.de:25',
            'mail_from_address': 'inostics-bioinformatics@sysmex-inostics.com',
            'receivers': 'gastaldello.francesco@sysmex-inostics.com'}
        base = os.path.split(os.path.split(__file__)[0])[0]
        data = read_config(os.path.join(base, 'tests', 'config.cfg'))
        self.assertDictEqual(data, referenceDict)

    def test_setup_logging(self):
        """Test assa.setup_logging."""
        logfile = r'tests/test_log.log'
        logger = setup_logging(logfile, level='info')
        logger.info('this line is an INFO')
        logger.error('this line is an ERROR')
        logger.debug('this line is a DEBUG')
        file_stats = os.stat(logfile)
        file_size = file_stats.st_size
        self.assertTrue(os.path.isfile(logfile))
        self.assertTrue(file_size > 0)
        os.remove(logfile)


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=3)
    unittest.main(testRunner=runner)
