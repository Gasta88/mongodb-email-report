#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Populate MongoDB in the unittest context."""

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
import sys
import json
from datetime import datetime, timezone


if __name__ == "__main__":
    client = MongoClient('mongodb://mongo/assa_testing')
    try:
        client.admin.command('ismaster')
    except ConnectionFailure:
        sys.exit("Server not available")
    db = client['assa_testing']
    with open('data/mongo_init.json') as f:
        file_data = json.load(f)
    for d in file_data:
        for k,v in d.items():
            if k == "date":
                d[k] = datetime.utcnow()
                break
    run_collection = db['Run']
    run_collection.insert_many(file_data)
    
    
    client.close()